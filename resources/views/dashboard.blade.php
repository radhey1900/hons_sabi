@extends('layouts.default')
@section('content')
<section class="content-header">
  <h1>
  Dashboard
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{$countUsers}}</h3>
          <p>Total Staffs</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-stalker"></i>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$countActiveClients}}</h3>
          <p>Active Clients</p>
        </div>
        <div class="icon">
          <i class="ion ion-cash"></i>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$countInactiveClients}}</h3>
          <p>Inactive Clients</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$countInvoices}}</h3>
          <p>Total Invoices</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="box box-danger">
        <div class="box-header with-border">
          <h3 class="box-title">Latest Clients</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="users-list clearfix">
              @foreach($latestcustomers as $customer)
                <li>
                    <img src="{{asset('/img/avatar2.png')}}" alt="User Image">
                    <a class="users-list-name" href="#">{{$customer->name}}</a>
                    <span class="users-list-date">{{$customer->type}}</span>
                </li>
              @endforeach
          </ul>
        </div>
        <div class="box-footer text-center">
          <a href="customer" class="uppercase">View All Clients</a>
        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Latest Invoice</h3>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table no-margin">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Plan</th>
                  <th>Renewed On</th>
                  <th>Expiry Date</th>
                  <th>Price (in RS)</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($latestinvoices as $invoices)
                    <tr>
                        <td><a href="{{url('profile/'.$invoices->customer_id.' ')}}">{{$invoices->username}}</a></td>
                        <td>{{$invoices->name}}</td>
                        <td>{{$invoices->plan_name}}</td>
                        <td>{{$invoices->renewed_on}}</td>
                        <td>{{$invoices->expiry_date}}</td>
                        <td>{{$invoices->total}}</td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="box-footer clearfix">
          <a href="statement" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
        </div>
      </div>
    </div>
  </div>
</section>
@stop