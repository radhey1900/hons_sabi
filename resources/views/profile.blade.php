@extends('layouts.default')
@section('content')
<section class="invoice">
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-globe"></i> Hons_Krishna
      <small class="label pull-right bg-green">Active</small>
      </h2>
    </div>
  </div>
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <address>
        <strong>Krishna Bahadur</strong><br>
        Chabahil, Kathmandu<br>
        <b>Phone: </b>(804) 123-5432<br>
        <b>Email: </b>info@krishna.com
      </address>
    </div>
    <div class="col-sm-4 invoice-col">
      <address>
        <strong>2 Mbps FTTH</strong><br>
        <b>Home User</b><br>
        <b>Installed Date: </b>10/09/2015
      </address>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Invoice</th>
            <th>Date</th>
            <th>Product</th>
            <th>Plan</th>
            <th>Expiry Date</th>
            <th>Description</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>12345</td>
            <td>10/09/2015</td>
            <td>Internet Installation</td>
            <td>1 Mbps FTTH </td>
            <td>10/09/2016</td>
            <td>Installment of Internet for 1 Year</td>
            <td>20000 /-</td>
          </tr><tr>
          <td>12587</td>
          <td>10/09/2016</td>
          <td>Upgrade Internet Plan</td>
          <td>2 Mbps FTTH </td>
          <td>10/12/2016</td>
          <td>Upgrade from 1 Mbps to 2 Mbps for 3 Months</td>
          <td>20000 /-</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="#" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
      <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
      <i class="fa fa-download"></i> Generate PDF
      </button>
    </div>
  </div>
</section>
@stop