@extends('layouts.default')
@section('content')
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-user" aria-hidden="true"></i> Edit Customer
      </h2>
    </div>
  </div>
  <!-- info row -->
 
  <!-- Table row -->
      <div class="row">
          <div class="col-xs-12">
            <form method="post" class="form-horizontal" action="{{url('customer/update')}}">
                <input type="hidden" name="id" value="{{$customer->id}}"/>
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <?php $name = isset($customer->name) ? $customer->name : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$name}}">
                  </div>
                </div>
                  <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <?php $email = isset($customer->email) ? $customer->email : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{$email}}">
                  </div>
                </div>
                  <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <?php $username = isset($customer->username) ? $customer->username : ''; ?>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{$username}}">
                  </div>
                </div>
                  <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone</label>
                  <?php $phone = isset($customer->phone) ? $customer->phone : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="{{$phone}}">
                  </div>
                </div>
                  <div class="form-group">
                  <label for="city" class="col-sm-2 control-label">City</label>
                  <?php $city = isset($customer->city) ? $customer->city : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{$city}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address</label>
                  <?php $address = isset($customer->address) ? $customer->address : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{$address}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-2 control-label">Type</label>
                  <?php $type = isset($customer->type) ? $customer->type : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="type" name="type" placeholder="Type" value="{{$type}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="installed_on" class="col-sm-2 control-label">Installed_on</label>
                  <?php $installed_on = isset($customer->installed_on) ? $customer->installed_on : ''; ?>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="installed_on" name="installed_on" placeholder="Installed_on" value="{{$installed_on}}">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="mac_address" class="col-sm-2 control-label">MAC Address</label>
                  <?php $mac_address = isset($customer->mac_address) ? $customer->mac_address : ''; ?>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="installed_on" name="mac_address" placeholder="MAC Address" value="{{$mac_address}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="refered_by" class="col-sm-2 control-label">Referred By</label>
                  <?php $refered_by = isset($customer->refered_by) ? $customer->refered_by : ''; ?>

                  <!--<select class="col-sm-10" name="refered_by" id="refered_by">
                  <option value=''>Select Refering Customer</option>
                    <?php foreach($allcustomers as $referer){ ?>
                      <option value={{ $referer->id }} <?php echo ($referer->id==$refered_by)?"selected":""; ?>>{{$referer->username}}</option>
                    <?php } ?>
                  </select> -->
                  
                  <div class="col-sm-10">
                    <select multiple class="form-control" name="refered_by" id="user_search" >
                      @if($allcustomers!='')
                      @foreach($allcustomers as $customer)
                      <option value="{{$customer->id}}"  <?php echo ($customer->id==$refered_by)?"selected":""; ?>>{{$customer->username}}</option>
                      @endforeach
                      @endif
                    </select>
                  </div>

                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Status</label>
                  <?php $name = isset($customer->name) ? $customer->name : ''; ?>
                  <div class="col-sm-10">
                    <?php $active = (isset($customer->status) && $customer->status == 1)?"selected":'';
                    $inactive = (isset($customer->status) && $customer->status == 0)?"selected":'';?>
                    <select name="status" class="form-control">
                          <option value="">Select Status</option>
                          <option value="1" {{$active}} >Active</option>
                          <option value="0" {{$inactive}} >Inactive</option>
                      </select>
                  </div>
                </div>
                <div class="form-group pull-right">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-default bg-green">Submit</button>
                </div>
             </div>
            </form>
          </div>
        </div>

</section>
@stop