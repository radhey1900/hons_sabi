@extends('layouts.default')
@section('content')
<section class="invoice">
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-globe"></i> {{ucfirst($customerProfile->username)}}
      @if($customerProfile->status==1)
      <small class="label pull-right bg-green">Active</small>
      @else
      <small class="label pull-right bg-red">InActive</small>
      @endif
      </h2>
    </div>
  </div>
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <address>
        <strong>{{$customerProfile->name}}</strong><br>
        {{$customerProfile->address}}, {{$customerProfile->city}}<br>
        <b>Phone: </b>{{$customerProfile->phone}}<br>
        <b>Email: </b>{{$customerProfile->email}}
      </address>
    </div>
    <div class="col-sm-4 invoice-col">
      <address>
        <!--        <strong>{{$customerProfile->plan}}</strong><br>-->
        <b>{{$customerProfile->type}}</b><br>
        <b>Installed Date: </b>{{$customerProfile->installed_on}}<br>
        <b>Mac Address: </b>{{$customerProfile->mac_address}}
      </address>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Invoice</th>
            <th>Date</th>
            <th>Payment Mode</th>
            <th>Plan</th>
            <th>Expiry Date</th>
            <th>Description</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
        <?php if(count($invoices)>0){?>
          @foreach($invoices as $invoice)
          <tr>
            <td>{{$invoice->code}}</td>
            <td>{{$invoice->payment_date}}</td>
            <td>{{$invoice->payment_mode}}</td>
            <td>{{$invoice->plan_name}} </td>
            <td>{{$invoice->expiry_date}}</td>
            <td>{{$invoice->description}}</td>
            <td>{{$invoice->total}} /-</td>
          </tr>
          @endforeach
          <?php }else{?>
          <tr>
          <td colspan="7">There are no invoices for this customer yet.</td>
          </tr>
            <?php }?>
        </tbody>
      </table>
    </div>
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="#" onclick="window.print()" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
        <a href="{{url('profilePdf/'.$customerProfile->id.' ')}}">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
          <i class="fa fa-download"></i> Generate PDF
          </button>
        </a>
      </div>
    </div>
  </div>
</section>
@stop