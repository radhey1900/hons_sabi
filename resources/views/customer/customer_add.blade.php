@extends('layouts.default')
@section('content')
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-user" aria-hidden="true"></i> Create Customer
      </h2>
    </div>
  </div>
  <!-- info row -->
 <!-- create clients -->

        <div class="row">
          <div class="col-xs-12">
           
            @if(isset($customera))
            <form method="post" class="form-horizontal" action="{{url('customer/update')}}">
                <input type="hidden" name="id" value="{{$customera->id}}"/>
            @else
            <form method="post" class="form-horizontal" action="{{url('customer')}}">
            @endif
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                   <?php $name = isset($customera->name) ? $customera->name : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" value="{{$name}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <?php $username = isset($customera->username) ? $customera->username : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{$username}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                   <?php $email = isset($customera->email) ? $customera->email : ''; ?>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$email}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="city" class="col-sm-2 control-label">City</label>
                   <?php $city = isset($customera->city) ? $customera->city : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{$city}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address</label>
                   <?php $address = isset($customera->address) ? $customera->address : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Detail Address" value="{{$address}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone</label>
                   <?php $phone = isset($customera->phone) ? $customera->phone : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name ="phone" id="phone" placeholder="Phone Number" value="{{$phone}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-2 control-label">Type</label>
                   <?php $type = isset($customera->type) ? $customera->type : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name ="type" id="type" placeholder="Type" value="{{$type}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="installed_on" class="col-sm-2 control-label">Installed On</label>
                   <?php $installed_on = isset($customera->installed_on) ? $customera->installed_on : ''; ?>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" name ="installed_on" id="installed_on" placeholder="yyyy-mm-dd" value="{{$installed_on}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="mac_address" class="col-sm-2 control-label">MAC Address</label>
                  <?php $mac_address = isset($customer->mac_address) ? $customer->mac_address : ''; ?>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="installed_on" name="mac_address" placeholder="MAC Address" value="{{$mac_address}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="refered_by" class="col-sm-2 control-label">Referred By</label>
                  <?php $refered_by = isset($customer->refered_by) ? $customer->refered_by : ''; ?>
                  <div class="col-sm-10">
                    <select multiple class="form-control" name="refered_by" id="user_search" >
                      @if($customers!='')
                      @foreach($customers as $customer)
                      <option value="{{$customer->id}}">{{$customer->username}}</option>
                      @endforeach
                      @endif
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="status" class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                      <select class="form-control" name="status">
                          <option value="1">Active</option>
                          <option value="0">InActive</option>
                      </select>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
          </form>
          </div>
        </div>
      
  <!-- Table row -->
      
</section>
@stop