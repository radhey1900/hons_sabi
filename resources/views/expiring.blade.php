@extends('layouts.default')
@section('content')
<section class="content-header">
    <h1>
    Expiring Soon Statements
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Product</th>
                                <th>Plan</th>
                                <th>Expires On</th>
                                <th>Location</th>
                                <th>Phone Number</th>
<!--                                <th>Action</th>-->
                            </tr>
                        </thead>
                        <tbody>
                             @foreach($invoices as $invoice)
                            <tr>
                                <td><a href="{{url('profile/'.$invoice->customer_id.' ')}}">{{$invoice->username}}</a></td>
                                <td>{{$invoice->name}}</td>
                                <td>{{$invoice->product}}</td>
                                <td>{{$invoice->plan}}</td>
                                <td>{{$invoice->expiry_date}}</td>
                                <td>{{$invoice->address}}</td>
                                <td>{{$invoice->phone}}</td>
<!--                                <td>
                                    <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#view-statement{{'.$invoice->id.'}}">View</button>
                                    @if(Auth::user()->role == 'admin')
                                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#edit-statement">Edit</button>
                                    @endif
                                </td>-->
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-print">
        <div class="col-xs-12">
           <a href="{{url('generate_expirings')}}">
                <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                    <i class="fa fa-download"></i> Generate Sheet
                </button>
            </a>
        </div>
    </div>
</section>
@include('pop-ups.pop_statement')
@stop