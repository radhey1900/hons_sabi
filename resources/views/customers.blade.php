@extends('layouts.default')
@section('content')
<section class="content-header">
    <h1>Customers</h1>
</section>
<section class="content">
    <div class="row margin-bottom">
        <div class="col-xs-12">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-client">Add Customer</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="tab-pane active" id="client">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Plan</th>
                                    <th>Phone </th>
                                    <th>Installed On</th>
                                    <th>Expires On</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                           
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('pop-ups.pop_clients')
@stop