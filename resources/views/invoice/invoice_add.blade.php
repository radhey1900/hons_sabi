@extends('layouts.default')
@section('content')
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-files-o" aria-hidden="true"></i> Create Invoice
      </h2>
    </div>
  </div>
  <!-- info row -->

        <div class="row">
          <div class="col-xs-12">
            <form method="post" class="form-horizontal" method="post" action="{{url('invoice')}}">
              <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="code" class="col-sm-2 control-label">Invoice Id</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="code" name="code" placeholder="Invoice Id" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <select multiple class="form-control" name="customer" id="user_search" required>
                      @foreach($customers as $customer)
                      <option value="{{$customer->id}}">{{$customer->username}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="product" class="col-sm-2 control-label">Product</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="product" name="product" placeholder="Product" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="plan" class="col-sm-2 control-label">Plan</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="plan">
                      <option value="">Select Plan</option>
                      @foreach($plans as $plan)
                      <option value="{{$plan->id}}">{{$plan->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="payment" class="col-sm-2 control-label">Payment Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="payment" name="payment_date" placeholder="yyyy-mm-dd" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="payment_mode" class="col-sm-2 control-label">Payment Mode</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="payment_mode" name="payment_mode" placeholder="Payment Mode" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="expiry" class="col-sm-2 control-label">Expiry Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="expiry" name="expiry_date" placeholder="yyyy-mm-dd" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="renewed_on" class="col-sm-2 control-label">Renewed On</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="renewed_on" name="renewed_on" placeholder="yyyy-mm-dd" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="description" name="description" placeholder="description"> </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Total</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="total" name="total" placeholder="Total Price In Rs" required="required">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
            </form>
          </div>
        </div>
      
  <!-- Table row -->
      
</section>
@stop