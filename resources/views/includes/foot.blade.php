<script src="{{ asset('/js/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('/js/jquery-ui.min.js') }}"></script>
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('/js/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('/js/jquery.knob.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('/js/daterangepicker.js') }}"></script>
<script src="{{ asset('/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/js/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/js/app.min.js') }}"></script>
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/select2.min.js') }}"></script>
<script src="{{ asset('/js/scripts.js') }}"></script>
  <script>
  $(function () {
    
   

    $("#example2").DataTable({
       "processing": true,
        "serverSide": true,
        "ajax": "{!! URL::asset('ajaxcustomer') !!}",
        "columns": [
            {data: 'username', name: 'username'},
            {data: 'name', name: 'name'},
            {data: 'plan', name: 'plan',"searchable": false,'sortable':false},
            {data: 'phone', name: 'phone'},
            {data: 'created_at', name: 'installed_on'},
            {data: 'expiry_date',name:'expiry_date',"searchable": false,'sortable':false},
            {data: 'status', name: 'status'},
            {
              name: 'action', 
              searchable:false,
              sortable: false,
              data:'action',
            } 
            
        ]
    });
  });

  </script>

<script>
  $('#datepickerFrom').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight:'TRUE',
    autoclose: true,
  });

  $('#datepickerTo').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight:'TRUE',
    autoclose: true,
  });
</script>


<script>
$(document).ready(function(){
  $('#user_search').select2({
    maximumSelectionLength: 1,
    placeholder:'Select Client'
  });
});

  $(document).ready(function(){
    /*$('#example')
        .on( 'order.dt',  function () { eventFired( 'Order' ); } )
        .on( 'search.dt', function () { eventFired( 'Search' ); } )
        .on( 'page.dt',   function () { eventFired( 'Page' ); } )
        .DataTable(); */
  });
  // funciton eventEired(eventob){
  //   alert(eventob);
  // }
  
</script>
<script src="{{ asset('/js/demo.js') }}"></script>
