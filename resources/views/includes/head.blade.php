
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HONS | Dashboard</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/AdminLTE.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/_all-skins.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/datepicker3.css') }}">
    <link href="{{url('/css/select2.css')}}" rel="stylesheet" />
    
    <!--script type="text/javascript" src="{{url('/js/jquery-2.2.3.min.js')}}"></script-->
  </head>