<?php  $userrole =  Auth::user()->role;
if($userrole == 'admin'){
    $menus = [
        'Dashboard' =>  [
            'url'   =>  'dashboard',
            'label' =>  'Dashboard',
            'class' =>  'fa fa-dashboard'
        ],
        'Statements' =>  [
            'url'   =>  'statement',
            'label' =>  'Statement',
            'class' =>  'fa fa-files-o'
        ],
        'Users' =>  [
            'url'   =>  'users',
            'label' =>  'Users',
            'class' =>  'fa fa-users'
        ],

        'Plan' =>  [
            'url'   =>  'plan',
            'label' =>  'Plans',
            'class' =>  'fa fa-cog'
        ],
        'Expiring Soon' =>  [
            'url'   =>  'expiring',
            'label' =>  'Expiring Soon',
            'class' =>  'fa fa-calendar'
        ],
    ];
}
if($userrole == 'staffs'){
    $menus = [
        'Dashboard' =>  [
            'url'   =>  'dashboard',
            'label' =>  'Dashboard',
            'class' =>  'fa fa-dashboard'
        ],
        'Statements' =>  [
            'url'   =>  'statement',
            'label' =>  'Statement',
            'class' =>  'fa fa-files-o'
        ],
        'Clients' =>  [
            'url'   =>  'customer',
            'label' =>  'Clients',
            'class' =>  'fa fa-users'
        ],
        'Expiring Soon' =>  [
            'url'   =>  'expiring',
            'label' =>  'Expiring Soon',
            'class' =>  'fa fa-calendar'
        ],
    ];
}

?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar left-side sidebar-offcanvas">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
              <img src="{{asset('/img/avatar.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <span> Welcome , {{ Auth::user()->name }}</span>
            </div>
         </div>
        <!-- sidebar menu: -->
        <ul class="sidebar-menu">
            @foreach($menus as $menu)
                <li{{ (strpos(Request::path(), $menu['url']) !== false) ? ' class=active' : '' }}>
                    <a href="{{{ URL::to($menu['url']) }}}"><i class="{{ $menu['class'] }}"></i> <span>{{ $menu['label'] }}</span></a>
                </li>
                
            @endforeach
            <!-- temp -->
            <li>
                <a href="#">
                   <i class="fa fa-users" aria-hidden="true"></i>
                    <span>Customers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        <li><a href="{{{ URL::to('add_customer') }}}"><i class="fa fa-check-circle" aria-hidden="true"></i> Add Customer</a></li>
                        <li><a href="{{{ URL::to('customer') }}}"><i class="fa fa-check-circle" aria-hidden="true"></i>List Customer</a></li>
                    </ul>
            </li>
            <li>
                <a href="#">
                   <i class="fa fa-files-o" aria-hidden="true"></i>
                    <span>Invoices</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        <li><a href="{{{ URL::to('add_invoice') }}}"><i class="fa fa-check-circle" aria-hidden="true"></i> Add Invoice</a></li>
                        <li><a href="{{{ URL::to('statement') }}}"><i class="fa fa-check-circle" aria-hidden="true"></i>List Invoices</a></li>
                    </ul>
            </li>
            <!-- temp -->
            <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out" aria-hidden="true"></i><span>Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
                
    </section>
    <!-- /.sidebar -->
</aside>
