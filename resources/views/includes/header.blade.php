<header class="main-header">
  <a href="index2.html" class="logo">
    <span class="logo-mini"><b>H</b></span>
    <span class="logo-lg"><b>HONS</b></span>
  </a>
  <nav class="navbar navbar-static-top">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
      
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">       
        <li class="dropdown user user-menu">
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <span class="hidden-xs">Logout</span>
                <i class="fa fa-sign-out" aria-hidden="true"></i>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
      </ul>
    </div>
  </nav>
</header>