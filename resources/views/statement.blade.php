@extends('layouts.default')
@section('content')
<section class="content-header">
    <h1>
        Statement
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12 margin-bottom">
            <button class="btn btn-success"  data-toggle="modal" data-target="#create-invoice">Add Invoice</button>
            
            <form method="POST" action="{{ url('exportxls') }}">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('dateFrom') ? 'has-error' : '' }} row">
                    <label class="col-sm-2 col-form-label">Date From:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="datepickerFrom" placeholder="Pick initial date..." name="dateFrom">
                    </div>
                    @if ($errors->has('dateFrom'))
                    <span class="help-block">
                        <strong class="text-danger">
                            {{ $errors->first('dateFrom') }}
                        </strong>
                    </span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('dateTo') ? 'has-error' : '' }} row">
                    <label class="col-sm-2 col-form-label">Date To:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="datepickerTo" placeholder="Pick last date..." name="dateTo">
                    </div>
                    @if ($errors->has('dateTo'))
                    <span class="help-block">
                        <strong class="text-danger">
                            {{ $errors->first('dateTo') }}
                        </strong>
                    </span>
                    @endif
                </div>
                
                <input type="hidden" value="" id="export_type" name="type" />
                <button class="btn bth-success" onclick="reportXLS()">Export</button>
            </form>

        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="typespecifier active"><a data-toggle="tab" data-type="internet" href="#internet">Internet</a></li>
                <li class="typespecifier"><a data-toggle="tab" data-type="nettv" href="#tv">Net Tv</a></li>
                <li class="typespecifier"><a data-toggle="tab" data-type="other" href="#other">Others</a></li>
            </ul>
            <div class="box">
                <div class="box-body">

                  <div class="tab-content">
                    <div id="internet" class="tab-pane fade in active">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Plan</th>
                                    <th>Billed On</th>
                                    <th>Expiry Date</th>
                                    <th>Payment Mode</th>
                                    <th>Price (in RS)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>    
                        </table>
                    </div>
                    <div id="tv" class="tab-pane fade">
                        <table id="nettvtable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Billed On</th>
                                    <th>Expiry Date</th>
                                    <th>Payment Mode</th>
                                    <th>Price (in RS)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>    
                        </table>
                    </div>
                    <div id="other" class="tab-pane fade">
                        <table id="othertable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Product Name</th>
                                    <th>Device Id</th>
                                    <th>Billed On</th>
                                    <th>Payment Mode</th>
                                    <th>Price (in RS)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>


@include('pop-ups.pop_invoice')
@stop


<script src="{{ asset('/js/jquery-2.2.3.min.js') }}"></script>

<script>
    $(document).ready(function(){


     $("#example1").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{!! URL::asset('ajaxinvoice/internet') !!}",
        "columns": [
        {data: 'username', name: 'username','sortable':false,'searchable':false},
        {data: 'plan', name: 'plan',"searchable": false,'sortable':false},
        {data: 'renewed_on', name: 'renewed_on'},
        {data: 'expiry_date', name: 'expiry_date'},
        {data: 'payment_mode',name:'payment_mode'},
        {data: 'total', name: 'total'},
        {
            name: 'action', 
            searchable:false,
            sortable: false,
            data:'action',
        } 

        ],

    }); 

     $("#nettvtable").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{!! URL::asset('ajaxinvoice/nettv') !!}",
        "columns": [
        {data: 'username', name: 'username','sortable':false,'searchable':false},
        {data: 'renewed_on', name: 'renewed_on'},
        {data: 'expiry_date', name: 'expiry_date'},
        {data: 'payment_mode',name:'payment_mode'},
        {data: 'total', name: 'total'},
        {
            name: 'action', 
            searchable:false,
            sortable: false,
            data:'action',
        } 

        ],
    }); 

     $("#othertable").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{!! URL::asset('ajaxinvoice/other') !!}",
        "columns": [
        {data: 'username', name: 'username','sortable':false,'searchable':false},

        {data: 'product_name', name: 'product_name'},
        {data: 'device_id', name: 'device_id'},
        {data: 'renewed_on',name:'renewed_on'},
        {data: 'payment_mode',name:'payment_mode'},
        {data: 'total', name: 'total'},
        {
            name: 'action', 
            searchable:false,
            sortable: false,
            data:'action',
        } 

        ]
    });   
 });

    function view_statement(id){
        var invoiceId = id;
        //alert(invoiceId);
        var __baseurl = "{{url('/')}}";
        $.ajax({
         url: __baseurl + '/ajaxGetInvoice',
         type: 'POST',
         data: {invoiceid: invoiceId, _token: "{{csrf_token()}}"},
         success: function(invoiceData) {
             jQuery("#view-statement").modal('show');
             $('.viewInvoice').html('');
             for(var i =0;i < invoiceData.length;i++)
             {
               var item = invoiceData[i];
               jQuery('#view-statement').find('.invoicecode').text("#"+item.code);
               $('.viewInvoice').append('<td>'+item.username+'</td>'
                +'<td>'+item.payment_date+'</td>'
                +'<td>'+item.product+'</td>'
                +'<td class="internet_field">'+item.plan+'</td>'
                +'<td class="internettv_field">'+item.expiry_date+'</td>'
                +'<td class="other_field">'+item.product_name+'</td>'
                +'<td class="other_field">'+item.device_id+'</td>'
                +'<td>'+item.added_by+'</td>'
                +'<td>'+item.total+'</td>');

               if(item.product!="internet"){
                if(item.product=="nettv"){
                    $(".internet_field").hide();
                    $(".other_field").hide();
                    $(".nettv_field").show();
                    $(".internettv_field").show();
                }else{
                    $(".internet_field").hide();
                    $(".internettv_field").hide();
                    $(".nettv_field").hide();
                    $(".other_field").show();
                }
            }else{
                $(".internet_field").show();
                $(".internettv_field").show();
                $(".nettv_field").hide();
                $(".other_field").hide();
            }

        } 
    }
});
        return false;
    }

//Edit Statment - to show the filled value by invoice id 
function edit_statement(id){
    var invoiceId = id;
       // alert(invoiceId);
       var __baseurl = "{{url('/')}}";
       $.ajax({
         url: __baseurl + '/ajaxGetInvoice',
         type: 'POST',
         data: {invoiceid: invoiceId, _token: "{{csrf_token()}}"},
         success: function(invoiceData) {
             jQuery("#edit-statement").modal('show');

             for(var i =0;i < invoiceData.length;i++)
             {
               var item = invoiceData[i];
               jQuery('#edit-statement').find('.invoicecode').text("#"+item.code);
               jQuery('#edit-statement').find('input[name="id"]').val(invoiceId);
               jQuery('#edit-statement').find('input[name="username"]').val(item.username);
               jQuery('#edit-statement').find('input[name="product"]').val(item.product);

               if(item.product=="internet"){
                jQuery('#edit-statement').find('input[name="plan"]').val(item.plan);
                jQuery('#edit-statement').find('input[name="plan"]').removeAttr("disabled");

                jQuery("#edit_product_name_field").hide();
                jQuery("#edit_device_id_field").hide();

                jQuery("#edit_product_name_field input").attr("disabled","disabled");
                jQuery("#edit_device_id_field input").attr("disabled","disabled");

            }

            else if(item.product=="nettv"){

               jQuery("#edit_plan_field").hide();
               jQuery("#edit_product_name_field").hide();
               jQuery("#edit_device_id_field").hide();
               jQuery('#edit-statement').find('input[name="plan"]').attr("disabled","disabled");

               jQuery("#edit_product_name_field input").attr("disabled","disabled");
               jQuery("#edit_device_id_field input").attr("disabled","disabled");

           }

           else if(item.product!="internet" && item.product!="nettv"){
               jQuery('#edit-statement').find('input[name="product_name"]').val(item.product_name);
               jQuery('#edit-statement').find('input[name="device_id"]').val(item.device_id);
               jQuery('#edit-statement').find('input[name="product_name"]').removeAttr("disabled");
               jQuery('#edit-statement').find('input[name="device_id"]').removeAttr("disabled");
               jQuery('#edit-statement').find('input[name="plan"]').attr("disabled","disabled");
               jQuery("#edit_plan_field").hide();
               jQuery("#edit_product_name_field").show();
               jQuery("#edit_device_id_field").show();

           }

           jQuery('#edit-statement').find('input[name="payment_date"]').val(item.payment_date);
           jQuery('#edit-statement').find('textarea[name="payment_mode"]').val(item.payment_mode);
           jQuery('#edit-statement').find('input[name="expiry_date"]').val(item.expiry_date);
           jQuery('#edit-statement').find('textarea[name="description"]').val(item.description);
           jQuery('#edit-statement').find('input[name="total"]').val(item.total);

       } 
   }
});
       return false;
   }


   function reportXLS(){
    var typevalue = "internet";
       // alert(invoiceId);
       var fromval="2017-11-17";
       var toval="2017-12-17";
       var __baseurl = "{{url('/')}}";


       $.ajax({
         url: __baseurl + '/exportxls',
         type: 'POST',
         data: {type: typevalue,from:fromval,to:toval, _token: "{{csrf_token()}}"},

     });
   }


</script>

<script type="text/javascript">
    function reportXLS(){
        var typestore = $(".typespecifier.active a").attr("data-type");
        if(typestore == "")
        {
            var type = "internet";
        }
        else
        {
            var type = typestore;
        }
        $("#export_type").val(type);
        $("#export_form").submit();
    }
</script>
