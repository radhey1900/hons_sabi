<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
    </head>
    <body>
        <section class="invoice">
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
              <i class="fa fa-globe"></i> {{$customerProfile->username}}
              </h2>
            </div>
          </div>
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              <address>
                <strong>{{$customerProfile->name}}</strong><br>
                {{$customerProfile->address}}, {{$customerProfile->city}}<br>
                <b>Phone: </b>{{$customerProfile->phone}}<br>
                <b>Email: </b>{{$customerProfile->email}}<br>
                 @if($customerProfile->status==1)
                <b>Status: </b><small>Active</small>
                @else
                <b>Status: </b><small>Inactive</small>
                @endif
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              <address> 
                <b>{{$customerProfile->type}}</b><br>
                <b>Installed Date: </b>{{$customerProfile->installed_on}}
              </address>
            </div>
          </div><br><br>
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped" border='1' cellspacing='0'>
                <thead>
                  <tr>
                    <th>Invoice</th>
                    <th>Date</th>
                    <th>Product</th>
                    <th>Plan</th>
                    <th>Expiry Date</th>
                    <th>Description</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{$invoice->code}}</td>
                            <td>{{$invoice->payment_date}}</td>
                            <td>{{$invoice->product}}</td>
                            <td>{{$invoice->plan_name}} </td>
                            <td>{{$invoice->expiry_date}}</td>
                            <td>{{$invoice->description}}</td>
                            <td>{{$invoice->total}}</td>
                          </tr>
                    @endforeach
              </tbody>
            </table>
          </div>
          </div>
        </section>

    </body>
</html>
