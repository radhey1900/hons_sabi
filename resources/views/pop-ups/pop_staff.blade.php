<!-- create clients -->
<div class="modal fade" id="create-staff" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create Staff</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">
          
            @if(isset($usera))
            <form method="post" class="form-horizontal" action="{{url('user/update')}}">
                <input type="hidden" name="id" value="{{$usera->id}}"/>
            @else
            <form method="post" class="form-horizontal" action="{{url('user')}}">
            @endif
           <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <?php $name = isset($usera->name) ? $usera->name : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" value="{{$name}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <?php $useraname = isset($usera->username) ? $usera->username : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{$useraname}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="role" class="col-sm-2 control-label">User Role</label>
                  <div class="col-sm-10">
                    <select class="form-control" id="role" value="" name="role" placeholder="Role">
                    <option value="staffs">Staff</option>
                    <option value="admin">Admin</option>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <?php $email = isset($usera->email) ? $usera->email : ''; ?>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{$email}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <?php $password = isset($usera->password) ? $usera->password : ''; ?>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="{{$password}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address</label>
                  <?php $address = isset($usera->address) ? $usera->address : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Detail Address" value="{{$address}}" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone</label>
                  <?php $phone = isset($usera->phone) ? $usera->phone : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" value="{{$phone}}" required="required">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>