<!-- Create Statement -->
<div class="modal fade" id="create-invoice" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create New Invoice for Client</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">
            <form method="post" class="form-horizontal" method="post" action="{{url('invoice')}}">
              <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="code" class="col-sm-2 control-label">Invoice Id</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="code" name="code" placeholder="Invoice Id" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <select multiple class="form-control" name="customer" id="user_search" required>
                      @foreach($customers as $customer)
                      <option value="{{$customer->id}}">{{$customer->username}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="product" class="col-sm-2 control-label">Product</label>
                  <div class="col-sm-10">
                    <select id="product" name="product" onchange="display_fields()">
                      <option value="internet">Internet</option>
                      <option value="nettv">Net Tv</option>
                      <option value="other">Other</option>
                    </select>
                  </div>
                </div>
                
                <div class="form-group internet_payment">
                          <label for="plan" class="col-sm-2 control-label">Plan</label>
                          <div class="col-sm-10">
                            <select class="form-control" name="plan">
                              <option value="">Select Plan</option>
                              @foreach($plans as $plan)
                              <option value="{{$plan->id}}">{{$plan->name}}</option>
                              @endforeach
                            </select>
                          </div>
                </div>
                <div class="form-group other_payment" style="display: none">
                  <label for="product_name" class="col-sm-2 control-label">Product Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="other_product_name" name="product_name" required="required" disabled="disabled">
                  </div>
                </div>
                <div class="form-group other_payment" style="display: none">
                  <label for="device_id" class="col-sm-2 control-label">Device ID</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="device_id" name="device_id" disabled="disabled">
                  </div>
                </div>

                
                <div class="form-group">
                  <label for="payment" class="col-sm-2 control-label">Payment Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="payment" name="payment_date" placeholder="yyyy-mm-dd" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="payment_mode" class="col-sm-2 control-label">Payment Mode</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="payment_mode" name="payment_mode" placeholder="Payment Mode" required="required">
                  </div>
                </div>
                <div class="form-group internettv_payment">
                  <label for="expiry" class="col-sm-2 control-label">Expiry Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="expiry" name="expiry_date" placeholder="yyyy-mm-dd" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="renewed_on" class="col-sm-2 control-label">Billed On</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="renewed_on" name="renewed_on" placeholder="yyyy-mm-dd" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="description" name="description" placeholder="description"> </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Total</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="total" name="total" placeholder="Total Price In Rs" required="required">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--- Edit statement -->

<div class="modal fade" id="edit-statement" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title invoicecode">#</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">
            <form method="post" class="form-horizontal" action="{{url('invoice/update')}}">
              <input type="hidden" class="form-control" id="invoice" name="id" placeholder="Id">
              <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="product" class="col-sm-2 control-label">Product</label>
                  <div class="col-sm-10">
                    <input readonly="" type="text" class="form-control" id="product" name="product" placeholder="Product">
                  </div>
                </div>

                
                <div class="form-group" id="edit_plan_field">
                  <label for="plan" class="col-sm-2 control-label">Plan</label>
                  <div class="col-sm-10">
                    <input disabled="disabled" type="text" class="form-control" id="plan" name="plan" placeholder="Plan">
                  </div>
                </div>

                <div class="form-group" id="edit_product_name_field">
                  <label for="plan" class="col-sm-2 control-label">Product Name</label>
                  <div class="col-sm-10">
                    <input disabled="disabled" type="text" class="form-control" id="product_name" name="product_name" placeholder="product name">
                  </div>
                </div>

                <div class="form-group" id="edit_device_id_field">
                  <label for="plan" class="col-sm-2 control-label">Device Id</label>
                  <div class="col-sm-10">
                    <input disabled="disabled" type="text" class="form-control" id="device_id" name="device_id" placeholder="Plan">
                  </div>
                </div>
              
                <div class="form-group">
                  <label for="payment" class="col-sm-2 control-label">Payment Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="payment_date" name="payment_date" placeholder="Payment Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="payment_mode" class="col-sm-2 control-label">Payment Mode</label>
                  <div class="col-sm-10">
                      <textarea class="form-control" id="payment_mode" name="payment_mode" placeholder="Payment Mode" required="required"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="expiry" class="col-sm-2 control-label">Expiry Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="expiry_date" name="expiry_date" placeholder="Expiry Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="description" name="description" placeholder="description"> </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Total</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="total" name="total" placeholder="Total Price In Rs">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- View Statement -->
<div class="modal fade" id="view-statement" role="dialog">
  
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title invoicecode">#</h4>
        <input type="hidden" name="data-view-id" id="data-view-id" value=""/>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Payment Date</th>
                  <th>Product</th>

                  <th class="internet_field">Plan</th>
                  
                  <th class="internettv_field">Expiry Date</th>
                  <th class="other_field">Product Name</th>
                  <th class="other_field">Device Id</th>
                  <th>Added By</th>
                  <th>Total(RS)</th>
                </tr>
              </thead>
              <tbody>
              <tr class="viewInvoice"></tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="modal" id="my_modal">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      <h4 class="modal-title">Modal header</h4>
    </div>
    <div class="modal-body">
      <p>some content</p>
      <input type="text" name="bookIdw" value=""/>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
<!--script src="{{url('/js/jquery-2.1.0.js')}}"></script>-->
<!--script src="{{url('/js/select2.min.js')}}"></script-->