<!-- Create Statement -->
<div class="modal fade" id="create-invoice" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">#54874</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="product" class="col-sm-2 control-label">Product</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="product" name="product" placeholder="Product">
                  </div>
                </div>
                <div class="form-group">
                  <label for="plan" class="col-sm-2 control-label">Plan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="plan" placeholder="Plan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="payment" class="col-sm-2 control-label">Payment Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="payment" name="payment" placeholder="Payment Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="expiry" class="col-sm-2 control-label">Expiry Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="expiry" name="expiry" placeholder="Expiry Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="description" placeholder="description"> </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Total</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="total" placeholder="Total Price In Rs">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--- Edit statement -->
<div class="modal fade" id="edit-statement" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">#54877</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12">
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                  </div>
                </div>
                <div class="form-group">
                  <label for="product" class="col-sm-2 control-label">Product</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="product" name="product" placeholder="Product">
                  </div>
                </div>
                <div class="form-group">
                  <label for="plan" class="col-sm-2 control-label">Plan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="plan" placeholder="Plan">
                  </div>
                </div>
                <div class="form-group">
                  <label for="payment" class="col-sm-2 control-label">Payment Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="payment" name="payment" placeholder="Payment Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="expiry" class="col-sm-2 control-label">Expiry Date</label>
                  <div class="col-sm-10">
                    <input type="date" class="form-control" id="expiry" name="expiry" placeholder="Expiry Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="description" placeholder="description"> </textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="total" class="col-sm-2 control-label">Total</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="total" placeholder="Total Price In Rs">
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-info pull-right">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- View Statement -->
<div class="modal fade" id="view-statement" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">#54874</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Username</th>
                  <th>Payment Date</th>
                  <th>Product</th>
                  <th>Plan</th>
                  <th>Expiry Date</th>
                  <th>Description</th>
                  <th>Total(RS)</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>hons_krishna</td>
                  <td>16/1/2017</td>
                  <td>Internet Installation</td>
                  <td>1Mbps Ftth</td>
                  <td>16/4/2017</td>
                  <td>Internet Installation for three months</td>
                  <td>1200</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>