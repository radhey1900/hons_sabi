@extends('layouts.default')
@section('content')
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-users" aria-hidden="true"></i> Edit User
      </h2>
    </div>
  </div>
  <!-- info row -->
 
  <!-- Table row -->
      <div class="row">
          <div class="col-xs-12">
            <form method="post" class="form-horizontal" action="{{url('user/update')}}">
            <input type="hidden" name="id" value="{{$user->id}}"/>
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <h4 class="box-title">User Profile</h4>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <?php $name = isset($user->name) ? $user->name : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$name}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <?php $username = isset($user->username) ? $user->username : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{$username}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-2 control-label">Phone</label>
                  <?php $phone = isset($user->phone) ? $user->phone : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="{{$phone}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-2 control-label">Address</label>
                  <?php $address = isset($user->address) ? $user->address : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{$address}}">
                  </div>
                </div>
                <div class="form-group pull-right">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-default bg-green">Submit</button>
                </div>
             </div>
            </form>
              
             
            <form method="post" class="form-horizontal" action="{{url('user/update')}}">
            <input type="hidden" name="id" value="{{$user->id}}"/>
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="box-body">
                <h4 class="box-title">Change Password</h4>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Email</label>
                  <?php $email = isset($user->email) ? $user->email : ''; ?>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{$email}}" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="">
                  </div>
                </div>
                 <div class="form-group pull-right">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-default bg-green">Submit</button>
                </div>
             </div>
            </form>
          </div>
        </div>

</section>
@stop