@extends('layouts.default')
@section('content')
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-file" aria-hidden="true"></i> Create New Plan
      </h2>
    </div>
  </div>
  <!-- info row -->
 
  <!-- Table row -->
      <div class="row">
          <div class="col-xs-12">
            @if(isset($plan))
            <form method="post" class="form-horizontal" action="{{url('plan/update')}}">
                <input type="hidden" name="id" value="{{$plan->id}}"/>
            @else
            <form method="post" class="form-horizontal" action="{{url('plan')}}">
            @endif
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <?php $name = isset($plan->name) ? $plan->name : ''; ?>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Plan Name" value="{{$name}}">
                  </div>
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-default">Cancel</button>
                  <button type="submit" class="btn btn-info pull-right">Submit</button>
                </div>
             </div>
            </form>
          </div>
        </div>

</section>
@stop