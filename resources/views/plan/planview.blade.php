@extends('layouts.default')
@section('content')
<section class="content-header">
    <h1>
    Plans
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12 margin-bottom">
            <a href="{{url('plan/create')}}"<button class="btn btn-success">Add Plan</button></a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plans as $plan)
                            <tr>
                                <td>{{$plan->id}}</td>
                                <td>{{$plan->name}}</td>
                                <td>
                                    <a href="{{url('plan/'.$plan->id.'/edit')}}"><button class="btn btn-sm btn-info">Edit</button></a>
                                   <a href="{{url('plan/'.$plan->id.'/delete')}}" onclick="return confirm('Are you sure?')"><button class="btn btn-sm btn-danger" disabled  >Delete</button></a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop