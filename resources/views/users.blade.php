@extends('layouts.default')
@section('content')
<section class="content-header">
    <h1>Users</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#staff" data-toggle="tab">Staff</a></li>
                    <li><a href="#client" data-toggle="tab">Customer</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="staff">
                        <div class="row margin-bottom">
                            <div class="col-xs-12">
                                <button class="btn btn-success"  data-toggle="modal" data-target="#create-staff">Add User</button>
                            </div>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Phone Number</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->username}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->role}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>{{$user->address}}</td>

                                        <td>
                                            <button class="btn btn-sm btn-info view_user" data-toggle="modal" data-target="#view-user" data-user_id="{{$user->id}}">View</button>
                                            <a href="{{url('user/'.$user->id.'/delete')}}" onclick="return confirm('Are you sure?')"><button class="btn btn-sm btn-danger">Delete</button></a>
                                            <a href="{{url('user/'.$user->id.'/edit')}}"><button class="btn btn-sm btn-info bg-green edit_user">Edit</button></a>
                                        </td>
                                    </tr>
                                    @endforeach
                               
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="client">
                        <div class="row margin-bottom">
                            <div class="col-xs-12">
                                <button class="btn btn-success" data-toggle="modal" data-target="#create-client">Add Customer</button>
                            </div>
                        </div>
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Plan</th>
                                    <th>Phone </th>
                                    <th>Installed On</th>
                                    <th>Expires On</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                                
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('pop-ups.pop_clients')
@include('pop-ups.pop_staff')
@include('pop-ups.pop_userview')
@stop

<script src="{{ asset('/js/jquery-2.2.3.min.js') }}"></script>
<script type='text/javascript'>
   jQuery .noConflict();
   jQuery(document).ready(function() {
       
//View User - to show the viewes user by user id 
    jQuery('.view_user').click(function(){
        var userId = $(this).attr('data-user_id');
         var __baseurl = "{{url('/')}}";
             $.ajax({
                     url: __baseurl + '/ajaxGetUser',
                     type: 'POST',
                     data: {userid: userId, _token: "{{csrf_token()}}"},
                     success: function(userData) { 
                         jQuery("#view-user").modal('show');
                         $('.viewUser').html('');
                         for(var i =0;i < userData.length;i++)
                         {
                           var item = userData[i];
                           // $('div.user_name').text(item.name);
                            $('.viewUser').append('<td>'+item.name+'</td>'
                                    +'<td>'+item.email+'</td>'
                                    +'<td>'+item.username+'</td>'
                                    +'<td>'+item.phone+'</td>'
                                    +'<td>'+item.address+'</td>'
                                    +'<td>'+item.role+'</td>');
                          } 
                     }
                 });
             return false;
         });

      });
</script>