@extends('layouts.default')
@section('content')
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
      <i class="fa fa-user"></i> Hons_krishna
      <small class="pull-right label bg-green">Active</small>
      </h2>
    </div>
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      <address>
        <strong>Krishna Bahadur</strong><br>
        Chabahil, Kathmandu<br>
        Phone: (977) 123-5432<br>
        Email: info@krishna.com
      </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <address>
        <strong>2Mbps ftth</strong><br>
        <b>Installed Date: </b> 10/9/2015<br>
      </address>
    </div>
  </div>
  <!-- /.row -->
  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Invoice</th>
            <th>Date</th>
            <th>Product</th>            
            <th>Plan</th>
            <th>Expiry Date</th>
            <th>Description</th>
            <th>Total(RS)</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>#12345</td>
            <td>10/9/2015</td>
            <td>Internet Installation</td>
            <td>1Mbps Ftth</td>
            <td>10/9/2016</td>
            <td>Installment of Internet for 1 year</td>
            <td>20000 /-</td>
          </tr>
          <tr>
            <td>#12384</td>
            <td>10/9/2016</td>
            <td>Upgrade Internet Plan</td>
            <td>2Mbps Ftth</td>
            <td>10/12/2016</td>
            <td>Upgrade of plan for 3 months</td>
            <td>3600 /-</td>
          </tr>
          <tr>
            <td>#54162</td>
            <td>10/12/2016</td>
            <td>Internet Payment</td>
            <td>2Mbps Ftth</td>
            <td>10/1/2017</td>
            <td>Payment for 1 month only</td>
            <td>1200 /-</td>
          </tr>
          <tr>
            <td>#89123</td>
            <td>16/1/2017</td>
            <td>Internet Payment</td>
            <td>2Mbps Ftth</td>
            <td>16/2/2017</td>
            <td>Payment for 1 month only</td>
            <td>1200 /-</td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>

</section>
@stop