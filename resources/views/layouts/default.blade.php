<!DOCTYPE html>
<html>
  @include('includes.head')
  <body class="hold-transition skin-blue sidebar-mini fixed">
    <div class="wrapper">
      @include('includes.header')
      @include('includes.menu')
      <div class="content-wrapper">
        @yield('content')
      </div>
      @include('includes.footer')
    </div>
    @include('includes.foot')
  </body>
</html>