<?php

namespace App\Models\Plan;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table='plans';
    protected $fillable=['name'];
    public $timestamps=true;
}
