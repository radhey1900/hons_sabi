<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table='invoices';
    protected $fillable=['customer_id','code','product','plan','payment_date','expiry_date','renewed_on','description','total','payment_mode','added_by','product_name','device_id'];
    public $timestamps=true;
}
