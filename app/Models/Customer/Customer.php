<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\Model;
use DB;

class Customer extends Model
{
    protected $table='customers';
    protected $fillable=['name','email','username','phone','city','address','type','installed_on','mac_address','refered_by','status'];
    public $timestamps=true;
    
    public function getCustomerProfile($id){
        $customerProfile = DB::table('customers')
                ->where('customers.id', '=', $id)
                ->join('invoices', 'customers.id', '=', 'invoices.customer_id')
                ->select('customers.*','customers.id','invoices.plan')
                ->first();
        return $customerProfile;
    }

    public function invoices(){
        return $this->hasMany('App\Models\Invoice\Invoice');
    }
     
}
