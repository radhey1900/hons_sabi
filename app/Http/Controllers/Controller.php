<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth,DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private $server_message = '';
    private $is_important = false;
    
    function loadpage($view, $pagetitle = 'Inspection Progress Management', $data = array()) {
        $data['pagetitle'] = $pagetitle;
        $data['SERVER_MESSAGE'] = $this->get_message();
        return view($view)->with($data);
    }

    function set_message($message = '') {
        $this->server_message = $message;
        return $this;
    }

    function important() {
        $this->is_important = true;
        return $this;
    }

    function get_message() {
        if (\Session::has('SERVER_MESSAGE')) {
            $server_message = \Session::get('SERVER_MESSAGE');
            \Session::put('SERVER_MESSAGE', '');
            \Session::forget('SERVER_MESSAGE');
        } else {
            $server_message = NULL;
        }
        return $server_message;
    }

    function success() {
        $server_message = $this->alert_template('fa-check', 'alert-success');
        $this->save_message($server_message);
        return $this;
    }

    function error() {
        $server_message = $this->alert_template('fa-ban', 'alert-danger');
        $this->save_message($server_message);
        return $this;
    }

    function info() {
        $server_message = $this->alert_template('fa-info', 'alert-info');
        $this->save_message($server_message);
        return $this;
    }

    function warning() {
        $server_message = $this->alert_template('fa-warning', 'alert-warning');
        $this->save_message($server_message);
        return $this;
    }

    function save_message($server_message) {
        if (\Session::has('SERVER_MESSAGE')) {
            $server_message .= \Session::get('SERVER_MESSAGE');
        }
        \Session::put('SERVER_MESSAGE', $server_message);
        $this->is_important = 'alert-dismissable';
    }

    function alert_template($icon, $class) {
        $message = '<div class="alert ' . $class . ' alert-dismissable" style="margin-left:10px;margin-right:10px;">';
        if (!$this->is_important):
            $message .='            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>';
        endif;
        $message .='            <span> <i class="icon fa ' . $icon . '"></i> <span>' . $this->server_message;
        $message .='</div>';
        return $message;
    }
}
