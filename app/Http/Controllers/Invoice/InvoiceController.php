<?php

namespace App\Http\Controllers\Invoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoice\Invoice;
use App\Models\Customer\Customer;
use App\Models\Plan\Plan;
use Yajra\Datatables\Facades\Datatables;
use DB,Auth;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;



class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$invoices = Invoice::all();

      $customers = Customer::all();
      $plans = Plan::all();
      return $this->loadpage('statement', 'List of Statements',compact('customers','plans'));
    }
    public function listinvoices($type){

      return Datatables::eloquent(Invoice::where('customer_id',"!=",'0'))
      ->where(function ($query) use ($type){ 
        if($type=="internet" || $type=="nettv"){
          $query->where('product', $type);
        }else{
          $query->where('product',"!=", "nettv")
          ->where('product',"!=","internet");
        }
      })

      ->select('invoices.*')
      ->addColumn("username",function($invoice){
        $customer=Customer::where('id',$invoice->customer_id)->first();
        if($customer){
          return $customer->username;
        }else{
          return "Deleted User";
        }
      })
      ->editColumn('plan',function($invoice){
        $plan=Plan::where('id',$invoice->plan)->first();
        if($plan)
          return $plan->name;
        else "";
      })
      ->addColumn('action',function($invoice){
       $acs='';


       $acs.='<button class="btn btn-sm btn-info view_statement" data-toggle="modal" data-target="#view-statement" onclick="view_statement('.$invoice->id.')" data-view_id="'.$invoice->id.'">View</button>';
       if(Auth::user()->role == 'admin'):
        $acs.=' <abutton class="btn btn-sm btn-success edit_statement" data-toggle="modal"  onclick="edit_statement('.$invoice->id.')" data-edit_id="'.$invoice->id.'">Edit</button>';
      endif;

      return $acs;
    })
      ->orderBy('invoices.renewed_on','Desc')

      ->make(true);
        //foreach($invoices as $invoice){

          //  $customer=Customer::find($invoice->customer_id);
           // if($customer){
           // $invoice->customer_name = $customer->username;
           // }
           // else{
           // $invoice->customer_name="User Deleted";
           // $invoice->user_is_deleted=true; 
           // }
           // $invoice->plan_name = Plan::find($invoice->plan)->name;
        //}
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $customers = Customer::all();
      $plans = Plan::all();
      return $this->loadpage('invoice.invoiceadd','create New Invoice',compact('customers','plans'));
    }
    /*temp*/
    public function addInvoice()
    {
      $customers = Customer::all();
      $plans = Plan::all();
      return $this->loadpage('invoice.invoice_add','create New Invoice',compact('customers','plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     $invoice['customer_id']= $request->get('customer');
     $invoice['code']= $request->get('code');
     $invoice['product']= $request->get('product');
     $invoice['plan']= $request->get('plan');
     $invoice['payment_date']= $request->get('payment_date');
     $invoice['payment_mode']= $request->get('payment_mode');
     $invoice['added_by']= Auth::user()->name;
     $invoice['expiry_date']= $request->get('expiry_date');
     $invoice['renewed_on']= $request->get('renewed_on');
     $invoice['description']= $request->get('description');
     $invoice['total']= $request->get('total');

     if($request->product=="other"){
       $invoice['product_name']=isset($request->product_name)?$request->get('product_name'):"";

       $invoice['device_id']=isset($request->device_id)?$request->get('device_id'):"";
     }

     Invoice::create($invoice);
     return redirect()->back();
       //return redirect('statement');
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $invoice=  Invoice::find($id);
      return $this->loadpage('invoice.invoiceadd','Edit Invoice',compact('invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
     $invoice = Invoice::find($request->id);
       //$invoice->customer_id = $request->customer_id;
     $invoice->code = $request->code;
     $invoice->product = $request->product;
      // $invoice->plan = $request->plan;
     $invoice->product_name=isset($request->product_name)?$request->get('product_name'):"";
     $invoice->device_id=isset($request->device_id)?$request->get('device_id'):"";
     $invoice->payment_date = $request->payment_date;
     $invoice->payment_mode= $request->payment_mode;
     $invoice->added_by= Auth::user()->name;
     $invoice->expiry_date = $request->expiry_date;
     $invoice->renewed_on = $request->renewed_on;
     $invoice->description = $request->description;
     $invoice->total = $request->total;
     $invoice->save();
     return redirect('statement');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $invoice =  Invoice::find($id);
      $invoice->delete();
      return redirect('statement');
    }
    
    public function expiringData()
    { 

      $today = Date('Y-m-d');
        //check date - today +15 days 

      $toData = date('Y-m-d', strtotime($today. ' + 15 days'));
        //now check expiring invoices which lies between $today and $toData

      $invoices =DB::table('invoices')
      ->join('customers','customers.id','=','invoices.customer_id')
      ->join('plans','plans.id','=','invoices.plan')
      ->whereBetween('expiry_date', [$today, $toData])
      ->select('invoices.*','customers.*','plans.name as plan')
      ->get();
      return $this->loadpage('expiring', 'List of Expiring Data', compact('invoices'));
    }
    
    public function csvExportExpiringData(){

      $today = Date('Y-m-d');
      $toData = date('Y-m-d', strtotime($today. ' + 15 days'));
      $invoices =DB::table('invoices')
      ->join('customers','customers.id','=','invoices.customer_id')
      ->join('plans','plans.id','=','invoices.plan')
      ->whereBetween('expiry_date', [$today, $toData])
      ->select('invoices.*','customers.*','plans.name as plan')
      ->get();

      $filename = "ExpiringData.csv";
      $handle = fopen($filename, 'w+');
      fputcsv($handle, array('Username', 'Name', 'Product', 'Plan', 'Expires On', 'Location', 'Phone'));
      foreach($invoices as $row) {
        fputcsv($handle, array($row->username,$row->name,$row->product,$row->plan,$row->expiry_date,$row->address, $row->phone));
      }
      fclose($handle);
      $headers = array(
        'Content-Type' => 'text/csv',
      );
      return response()->download($filename, 'ExpiringData '.date("d-m-Y H:i").'.csv', $headers);

//    fopen(public_path('expiringcsv'), 'w');
//    $file = fopen('csv','file.csv', 'w');
//    foreach ($invoices as $row) {
//        fputcsv($file, array($row->username,$row->name,$row->product,$row->plan,$row->expiry_date,$row->address, $row->phone));
//    }
//    fclose($file);
      
    }
    
    public function ajaxGetInvoice(Request $request){

      $invoiceid = $request->get('invoiceid');
      $inv=Invoice::where("id",$invoiceid)->first();
      if($inv){


        if($inv->product=="internet"){


          $invoices =DB::table('invoices')        
          ->join('customers','customers.id','=','invoices.customer_id')
          ->join('plans','plans.id','=','invoices.plan')
          ->where('invoices.id',$invoiceid)
          ->select('invoices.*','customers.*','plans.name as plan')
          ->take(10)->get();

          return $invoices;
        }
        else{
          $invoices =DB::table('invoices')
          ->join('customers','customers.id','=','invoices.customer_id')
          ->where('invoices.id',$invoiceid)
          ->select('invoices.*','customers.*')
          ->take(10)->get();
          return $invoices;
        }
        
        return $invoices;
      }
    }

    public function ajaxExportPaymentExl(Request $request)
    {
      $this->validate($request, [
            'dateFrom' => 'required',
            'dateTo' => 'required',
        ]);

      $from=date('Y-m-d', strtotime($request->dateFrom));
      $to=date('Y-m-d', strtotime($request->dateTo));

      $type="";
      if(isset($request->from)){
        $from=$request->from;
      }
      if(isset($request->to)){
        $to=$request->to;
      }
      if(isset($request->type)){
        $type=$request->type;
      }
      $invoices="";
      if($from!="" && $to!=""){

        if($type=="internet"){
          $invoices =DB::table('invoices')
          ->join('customers','customers.id','=','invoices.customer_id')
          ->where('product','=','internet')
          ->whereBetween('payment_date',array($from,$to))
          ->select('invoices.*','customers.*')
          ->get();
        }
        else if($type=="nettv"){
          $invoices =DB::table('invoices')
          ->join('customers','customers.id','=','invoices.customer_id')
          ->where('product','=','nettv')
          ->whereBetween('payment_date',array($from,$to))
          ->select('invoices.*','customers.*')
          ->get();
        }
        else if($type=="other"){
          $invoices =DB::table('invoices')
          ->join('customers','customers.id','=','invoices.customer_id')
          ->where('product','!=','internet')
          ->where('product','!=','nettv')
          ->whereBetween('payment_date',array($from,$to))
          ->select('invoices.*','customers.*')
          ->get();
        }
        else {
          $invoices =DB::table('invoices')
          ->join('customers','customers.id','=','invoices.customer_id')
          ->whereBetween('payment_date',array($from,$to))
          ->select('invoices.*','customers.*')
          ->get();
        }

        $data = json_decode(json_encode($invoices), True);
        return Excel::create('Filename', function($excel) use ($data) {
          $excel->sheet('mySheet', function($sheet) use ($data)
          {
            $sheet->fromArray($data);
          });
        })->download('xls');

        if($invoices!="" && $invoices){

        } 

      }else{
        return false;
      }
    }

  }


