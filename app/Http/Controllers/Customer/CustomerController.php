<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Customer\Customer;
use App\Models\Plan\Plan;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Invoice\Invoice;
use App\User;
use DB;
use Dompdf\Dompdf;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    //show only customers 
    public function index()
    {
        //$customers = Customer::all();
        $customers=array();
        return $this->loadpage('customers', 'List of Customers', compact('customers'));
    }
    
    public function listcustomers(){
     
        
        return Datatables::eloquent(
            Customer::where('customers.id','!=',0)
        /*->join('invoices',function($join){
            return $join->on('invoices.customer_id', '=', 'customers.id')
            ->orderBy('invoices.id','desc');
            //->where('invoices.id','=',DB::raw('(select max(id) from invoices where invoices.customer_id = customers.id)'));
        })
        ->join('plans','plans.id','=','invoices.plan')*/
        
        )->select('customers.*')
        ->addColumn('plan',function($customer){
            $invoice=$this->getInvoice($customer->id);
            if($invoice)
            return $invoice->plan;
            else "";
        })
        ->addColumn('expiry_date',function($customer){
            $invoice=$this->getInvoice($customer->id);
            if($invoice)
            return $invoice->expiry_date;
            else "";
            })
       ->addColumn('action',function($customer){
           $acs='';
           $link=url('profile/'.$customer->id);
           $acs.='<a href="'.$link.'"><button class="btn btn-sm btn-info">View</button></a>';

           $link=url('customer/'.$customer->id.'/delete');
           $acs.='<a onclick="return confirm(\'Are you sure?\')" href="'.$link.'"><button class="btn btn-sm btn-danger">Delete</button></a>';
           $link=url('customer/'.$customer->id.'/edit');
           $acs.='<a href="'.$link.'"><button class="btn btn-sm btn-info bg-green edit_customer">Edit</button></a>';
                                          //  <a href="{{url('customer/'.$customer->id.'/delete')}}" onclick="return confirm('Are you sure?')"><button class="btn btn-sm btn-danger">Delete</button></a>
                                           // <a href="{{url('customer/'.$customer->id.'/edit')}}"><button class="btn btn-sm btn-info bg-green edit_customer">Edit</button></a>
                                       
           return $acs;
       })
       ->editColumn('status', function($customer){
           if($customer->status=="1"){
               return "active";
               }
           else{
            return "inactive";
           } 
       })
        ->orderBy('customers.name','asc')
        ->distinct('customers.id')
            ->make(true);
    }


    //show  customers and staffs 
    public function showUsers()
    {
        //$customers = Customer::where('id','!=',0)->select('id','username','name','phone','created_at','status')
        //->get();

            //foreach($customers as $customer){
                //$invoices =  $this->getInvoice($customer->id);
                //if(($invoices!="")){
                 //   $customer->invoices = $invoices;
                //}
            //}
        $customers = Customer::all();
        $users = User::all();
        return $this->loadpage('users', 'List of Users', compact('users','customers'));
    }

    /*To get a single invoice latest inserted */
    public function getInvoice($customer_id){
       // $invoice = Invoice::where('customer_id',$customer_id)->orderBy('id','DESC')->first();
        $invoice = Invoice::where('customer_id',$customer_id)
                ->join('plans','plans.id','=','invoices.plan')
                ->orderBy('id','DESC')
                ->select('invoices.*','plans.name as plan')
                ->first();
        return $invoice;
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return $this->loadpage('customer.customeradd','create New Customer');
    }

    /*temp*/
    public function addCustomer()
    {        
        $customers = Customer::all();
        return $this->loadpage('customer.customer_add','create New Customer', compact('customers'));
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer['name'] = $request->get('name');
        $customer['email'] = $request->get('email');
        $customer['username'] = $request->get('username');
        $customer['phone'] = $request->get('phone');
        $customer['city'] = $request->get('city');
        $customer['address']= $request->get('address');
        $customer['type']= $request->get('type');
        $customer['installed_on']= $request->get('installed_on');
        $customer['status']= $request->get('status');
        $customer['mac_address']=$request->get('mac_address');
        $customer['refered_by']= $request->get('refered_by');
        Customer::create($customer);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer =  Customer::find($id);
        $allcustomers = Customer::all('username','id')->except($id);
      
        return $this->loadpage('customer.customer_edit','Edit Customer',compact('customer','allcustomers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
       $customer = Customer::find($request->id);
       $customer->name=$request->name;
       $customer->email=$request->email;
       $customer->username=$request->username;
       $customer->phone=$request->phone;
       $customer->city=$request->city;
       $customer->address=$request->address;
       $customer->type= $request->type;
       $customer->installed_on= $request->installed_on;
       $customer->status= $request->status;
       $customer->mac_address=$request->mac_address;
       $customer->refered_by=$request->refered_by;
       $customer->save();
       return redirect('customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $customer =  Customer::find($id);
      $customer->delete();
      return redirect()->back();
    }
    
    public function profile($id)
    {
      $customerObj = new Customer;
      $customerProfile =  Customer::find($id);//$customerObj->getCustomerProfile($id);
      $invoices = $customerProfile->invoices;//Invoice::where('customer_id',$id)->get();
      foreach($invoices as $invoice){
            $invoice->plan_name = Plan::find($invoice->plan)->name;
        }
        
      return $this->loadpage('customer.profile','Customer Profile',compact('customerProfile','invoices'));

    }
    
    public function profileGeneratePDF($id)
    {
        
        $customerObj = new Customer;
        $customerProfile =  $customerObj->getCustomerProfile($id);
        $invoices = Invoice::where('customer_id',$id)->get();
        foreach($invoices as $invoice){
              $invoice->plan_name = Plan::find($invoice->plan)->name;
          }
       
         $pdf = new Dompdf();
         $pdf->setPaper('A4', 'landscape');
         $pdf = \PDF::loadView('customer.profile_pdf',['customerProfile' => $customerProfile, 'invoices' => $invoices]);
         return $pdf->stream('profile');

    }
}
