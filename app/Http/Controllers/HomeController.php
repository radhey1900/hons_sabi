<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Customer\Customer;
use App\Models\Invoice\Invoice;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return view('home');
        return $this->dashboard();
    }
    
    public function dashboard(){
        $countUsers = User::all()->count();
        $countActiveClients = Customer::where('status','1')->get()->count();
        $countInactiveClients = Customer::where('status','0')->get()->count();
        $countInvoices = Invoice::all()->count();
        $latestcustomers= Customer::orderBy('id', 'desc')->take(8)->get();
        $latestinvoices =DB::table('invoices')->orderBy('invoices.id','desc')
                ->join('customers','customers.id','=','invoices.customer_id')
                ->join('plans','plans.id','=','invoices.plan')
                ->select('customers.*','invoices.*','plans.name as plan_name')
                ->take(10)->get();
        return $this->loadpage('dashboard', 'Dashboard', compact('latestcustomers','latestinvoices','countUsers','countActiveClients','countInactiveClients','countInvoices'));
    }
    
}
