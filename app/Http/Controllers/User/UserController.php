<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user['name'] = $request->get('name');
        $user['email'] = $request->get('email');
        $user['username'] = $request->get('username');
        $user['password']= bcrypt($request->get('password'));
        $user['phone'] = $request->get('phone');
        $user['address']= $request->get('address');
        $user['role']= $request->get('role');
        User::create($user);
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $userid = $request->get('userid');
        $user =  User::where('id',$userid)->get();
        return $user;
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =  User::find($id);
        return $this->loadpage('user.user_edit','Edit User',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
       $user = User::find($request->id);
       $user->name = (($request->name!='')?$request->name:$user->name);
       $user->username = (($request->username!='')?$request->username:$user->username);
       $user->phone = (($request->phone!='')?$request->phone:$user->phone);
       $user->address = (($request->address!='')?$request->address:$user->address);
       $user->password = (($request->password!='')?bcrypt($request->password):$user->password);
       $user->save();
       return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user =  User::find($id);
      $user->delete();
      return redirect('users');
    }
}
