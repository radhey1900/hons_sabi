-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2017 at 07:10 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hons`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `installed_on` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `username`, `phone`, `city`, `address`, `type`, `installed_on`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Shyam', 'shyam@gmail.com', 'hons_shyam', '34567', 'ktm', 'ktm', 'Home User', '2017-03-18', 1, '2017-03-18 04:28:38', '2017-03-18 04:28:38'),
(3, 'Ram', 'ram@gmail.com', 'hons_ram', '34567', 'ktm', 'dcfvgbhj', 'Office User', '2017-03-19', 0, '2017-03-18 04:51:36', '2017-03-18 04:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `code` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry_date` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `renewed_on` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `customer_id`, `code`, `product`, `plan`, `payment_date`, `expiry_date`, `renewed_on`, `description`, `total`, `created_at`, `updated_at`) VALUES
(1, 3, 'I01', 'test', '1', '2017-03-17', '2017-12-19', NULL, 'test', 1212, '2017-03-18 05:26:56', '2017-03-18 05:26:56'),
(2, 2, 'I02', 'test', '2', '2017-03-09', '2017-03-22', NULL, 'test 123', 10000, '2017-03-18 05:33:56', '2017-03-18 05:33:56'),
(3, 3, 'I03', 'shya', '3', '2017-03-03', '2017-03-04', NULL, 'test for shyam invoice', 10000, '2017-03-18 10:48:59', '2017-03-18 10:48:59'),
(4, 2, 'I04', '234', '5', '2017-03-04', '2017-03-04', NULL, '2345', 343434, '2017-03-18 11:03:09', '2017-03-18 11:03:09'),
(5, 3, 'I05', 'sdfg', '6', '2017-03-01', '2017-03-16', '2017-03-22', 'wedrft', 3456, '2017-03-18 11:03:43', '2017-03-18 11:03:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_03_17_182119_create_table_customers', 2),
(4, '2017_03_17_182224_create_table_plans', 2),
(5, '2017_03_17_182242_create_table_invoices', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('mahsabi130@gmail.com', '$2y$10$OHaoZRZCyKlh9vO04VQOs.251RlyWOVnumZNs1yxLzJhdLGXchIBC', '2017-03-18 11:26:32');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, '1Mbps ftth', NULL, NULL),
(2, '2Mbps ftth', NULL, NULL),
(3, '5Mbps ftth', NULL, NULL),
(5, '15 Mbps ftth', '2017-03-18 10:23:36', '2017-03-18 10:31:57'),
(6, '10 Mbps ftth', '2017-03-18 10:33:01', '2017-03-18 10:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('admin','staffs') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `phone`, `address`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sabi maharjan', 'mahsabi130@gmail.com', 'sabi', '$2y$10$oi3lNNIFtrKUEjVOO11oZuJ6GqAbijns8c1edMN3zYwRMQwKZbcq6', '9849100816', 'Patan', 'admin', '4QSKPUgIIc6oQu2EvRAPsAmhHTgudtK5jY1yF91d8BgDL7l7LtGtuS2gmHby', '2017-03-17 11:59:03', '2017-03-17 11:59:03'),
(5, 'Rahul', 'rahul@gmail.com', 'rahul', '$2y$10$sYTipDWDjksigzaRHrO2wenk/x6WfQJ0NYm1YLMMiAfE30EtItUaC', '23456', 'ktm', 'staffs', 'YZYzgZiNP8g7MTJSg5eAua3XgJZNWEOhKsdl11xaYAXWF9OA9wrQBDAEzLpV', '2017-03-18 04:50:56', '2017-03-18 04:50:56'),
(7, 'sabee', 'sabee@gmail.com', 'sabee', '$2y$10$XiHPqBiMy8mSWZ/Q4t198u3FgfCum7WiEBEC/QbYetW22ceXMRKu6', '345678', 'gf', 'staffs', 'va5PskcmsKce6T5JLAothNGvL0rSrlyQMPW5iEAEME5wvdz8xMqOyWiTnnJ8', '2017-03-18 11:28:09', '2017-03-18 11:28:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
