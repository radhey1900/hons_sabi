<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('login', function () {
    return view('login');
});

//Route::get('dashboard', function () {
//    return view('dashboard');
//});


//Route::get('expiring', function () {
//    return view('expiring');
//});

//Route::get('dashboard', function () {
//    return view('dashboard');
//});
//Route::get('statement', function () {
//    return view('statement');
//});
//Route::get('invoice', function () {
//    return view('invoiceview');
//});
//Route::get('users', function () {
//    return view('users');
//});
//Route::get('profile', function () {
//    return view('profile');
//});




///sabi
Auth::routes();

Route::get('/dashboard', 'HomeController@dashboard');


Route::get('/home', 'HomeController@index');

Route::get('/', function () {
    if(Auth::check()){
       return redirect('/dashboard');
    }
    else{
    return view('auth/login');
    }
});

    /*customers */
    Route::get("users", "Customer\CustomerController@showUsers");
    Route::get("ajaxcustomer", "Customer\CustomerController@listcustomers");
    Route::get("ajaxinvoice/{type}","Invoice\InvoiceController@listinvoices");
    

    Route::resource("customer", "Customer\CustomerController");
    Route::get("customer/{id}/delete", "Customer\CustomerController@destroy");
    Route::get("customer/{id}/edit", "Customer\CustomerController@edit");
    Route::post("customer/update", "Customer\CustomerController@update");
    
    Route::get("add_customer", "Customer\CustomerController@addCustomer");
    
    /*users - staffs/admin */
    Route::resource("user", "User\UserController");
    Route::get("user/{id}/delete", "User\UserController@destroy");
    Route::get("user/{id}/edit", "User\UserController@edit");
    Route::post("user/update", "User\UserController@update");
    Route::post("ajaxGetUser", "User\UserController@show");
    
    /*invoice */
    Route::resource("invoice", "Invoice\InvoiceController");
    Route::get("invoice/{id}/delete", "Invoice\InvoiceController@destroy");
    Route::get("invoice/{id}/edit", "Invoice\InvoiceController@edit");
    Route::post("invoice/update", "Invoice\InvoiceController@update");
    Route::post("ajaxGetInvoice", "Invoice\InvoiceController@ajaxGetInvoice");
    
    /*temp*/ Route::get("add_invoice", "Invoice\InvoiceController@addInvoice");

    /*statement */
    Route::get("statement", "Invoice\InvoiceController@index");
    Route::get("profile/{id}", "Customer\CustomerController@profile");
    Route::get("profilePdf/{id}", "Customer\CustomerController@profileGeneratePDF");
    Route::post("exportxls/","Invoice\InvoiceController@ajaxExportPaymentExl");
    
    
    /*plan */
    Route::resource("plan", "Plan\PlanController");
    Route::get("plan/{id}/delete", "Plan\PlanController@destroy");
    Route::get("plan/{id}/edit", "Plan\PlanController@edit");
    Route::post("plan/update", "Plan\PlanController@update");
    
    /*Expiring soon*/
    Route::get("expiring", "Invoice\InvoiceController@expiringData");
    Route::get("generate_expirings", "Invoice\InvoiceController@csvExportExpiringData");

